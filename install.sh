#!/bin/bash
echo "Enter your email: "  
read YOUR_EMAIL
echo "Enter your name for git"
read YOUR_NAME

sudo apt update

sudo apt install dosfstools mtools mtd-utils default-jre default-jdk python3.8 python3-setuptools python3-pip git git-lfs zip scons libncurses5 ruby -y

sudo rm -f /bin/sh
sudo ln -s /bin/bash /bin/sh

if [ -f /usr/bin/python ]; then
sudo rm /usr/bin/python
fi
sudo ln -s /usr/bin/python3 /usr/bin/python

sudo pip3 install setuptools kconfiglib pycryptodome ecdsa

sudo pip3 install six

# arg1=download URL; arg2=sha256 checksum URL
# ret downloaded(0), already exist checksum right(1), download fialed(3)
function DownloadAndCheck() {
checkSumFile=`basename $2`
downloadFile=`basename $1`
echo "downloadFile=$downloadFile"
echo "checksumFile=$checkSumFile"
if [ -f $downloadFile ]; then
curl $2 > $checkSumFile
#echo " $downloadFile" >> $checkSumFile
checkSum=`cat $checkSumFile`" $downloadFile"
echo $checkSum
echo $checkSum | sha256sum -c --status
if [ $? -eq 0 ]; then
return 1
else
rm $downloadFile
fi
fi

wget $1
sha256sum -c --status $checkSumFile
if [ $? -eq 0 ]; then
return 0
else
return 3
fi
}

DownloadAndCheck "https://repo.huaweicloud.com/harmonyos/compiler/gn/1717/linux/gn-linux-x86-1717.tar.gz" "https://repo.huaweicloud.com/harmonyos/compiler/gn/1717/linux/gn-linux-x86-1717.tar.gz.sha256"
mkdir -p ~/gn
tar -xvf gn-linux-x86-1717.tar.gz -C ~/gn/

DownloadAndCheck "https://repo.huaweicloud.com/harmonyos/compiler/ninja/1.9.0/linux/ninja.1.9.0.tar" "https://repo.huaweicloud.com/harmonyos/compiler/ninja/1.9.0/linux/ninja.1.9.0.tar.sha256"
tar -xvf ninja.1.9.0.tar -C ~/

if [ -f hapsigntoolv2.jar ]; then
rm hapsigntoolv2.jar
fi
wget https://repo.huaweicloud.com/harmonyos/develop_tools/hapsigntoolv2.jar
mkdir -p developtools
mv hapsigntoolv2.jar developtools/hapsigntoolv2.jar

DownloadAndCheck "https://repo.huaweicloud.com/harmonyos/compiler/clang/10.0.1-53907/linux/llvm.tar.gz" "https://repo.huaweicloud.com/harmonyos/compiler/clang/10.0.1-53907/linux/llvm.tar.gz.sha256"
tar -xvf llvm.tar.gz -C ~/


DownloadAndCheck "https://repo.huaweicloud.com/harmonyos/compiler/hc-gen/0.65/linux/hc-gen-0.65-linux.tar" "https://repo.huaweicloud.com/harmonyos/compiler/hc-gen/0.65/linux/hc-gen-0.65-linux.tar.sha256"
tar -xvf hc-gen-0.65-linux.tar -C ~/

DownloadAndCheck "https://repo.huaweicloud.com/harmonyos/compiler/gcc_riscv32/7.3.0/linux/gcc_riscv32-linux-7.3.0.tar.gz" "https://repo.huaweicloud.com/harmonyos/compiler/gcc_riscv32/7.3.0/linux/gcc_riscv32-linux-7.3.0.tar.gz.sha256"
tar -xvf gcc_riscv32-linux-7.3.0.tar.gz -C ~

echo "export PATH=~/gn:\$PATH" >> ~/.bashrc
echo "export PATH=~/ninja:\$PATH" >> ~/.bashrc
echo "export PATH=~/llvm/bin:\$PATH" >> ~/.bashrc
echo "export PATH=~/hc-gen:\$PATH" >> ~/.bashrc
echo "export PATH=~/llvm/bin:\$PATH" >> ~/.bashrc
echo "export PATH=~/developtools:\$PATH" >> ~/.bashrc
echo "export PATH=~/.local/bin:\$PATH" >> ~/.bashrc
echo "export PATH=~/gcc_riscv32/bin:\$PATH" >> ~/.bashrc
source ~/.bashrc

if [ -f repo-py3 ]; then
rm repo-py3
fi
wget https://gitee.com/oschina/repo/raw/fork_flow/repo-py3
sudo mv repo-py3 /usr/local/bin/repo

sudo chmod a+x /usr/local/bin/repo
sudo pip3 install -i https://pypi.tuna.tsinghua.edu.cn/simple requests
python3 -m pip install --user ohos-build

git config --global user.name "$YOUR_NAME"
git config --global user.email "$YOUR_EMAIL"
git config --global credential.helper store

if [ ! -f ~/.ssh/id_rsa.pub ]; then
ssh-keygen -t rsa -C "$YOUR_EMAIL"
fi

echo "Set the following PublicKey to gitee and press enter to continue"
cat ~/.ssh/id_rsa.pub
read READY
echo "Checking PublicKey..."
ssh -T git@gitee.com
