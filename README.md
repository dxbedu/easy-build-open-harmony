# EasyBuildOpenHarmony

#### 介绍
一个用于快速搭建OpenHarmony编译环境的脚本库

#### 使用说明

1.  下载相应shell脚本到用户目录
2.  bash install.sh
3.  按提示输入邮箱、git 昵称

开始repo init下载代码，一起玩转OpenHarmony吧

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

